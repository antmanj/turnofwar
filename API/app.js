//Entry point for TurnOfWar REST API

var express = require('express'),
    mongoose = require('mongoose'),
    everyauth = require('everyauth'),
    UserHandler = require('./models/user'),
    utils = require('./utils'),
    config = utils.config;

//DB init
function initializeDatabase() {
    mongoose.connection.on("open", function() {
        console.log("Connected to MongoDB successfully!");
    });

    mongoose.connect(utils.generateMongoUrl(getMongoOptions()));
}

//Get mongodb options, if deployed get from VCAP env variables. otherwise from config file 
function getMongoOptions() {
    if(process.env.VCAP_SERVICES){
        var env = JSON.parse(process.env.VCAP_SERVICES);
        return env['mongodb-1.8'][0]['credentials'];
    }
    else{
        return {
            "hostname": config.get('database:hostname') || 'localhost',
            "port": config.get('database:port') || 27017,
            "username": config.get('database:username') || '',
            "password": config.get('database:password') || '',
            "name": config.get('database:name') || '',
            "db": config.get('database:db') || 'TurnOfWar'
        }
    }
}

function getRedisOptions(){
    return {
        "host": config.get('redis:host') || 'localhost',
        "port": config.get('redis:port') || 6379,
        "user": config.get('redis:user') || '',
        "pass": config.get('redis:pass') || ''
    }
    
}

//Read all files from routes directory and load the files, if registerRoutes function in exported execute it 
function registerRoutes(app) {
    require("fs").readdirSync("./routes").forEach(function(file) {
        if(file.match('js$')=='js') {
            var route = require("./routes/" + file);
            if(route && route.registerRoutes) {
                route.registerRoutes(app);
                console.log(file + ': routes registred');
            }
        }
    });
}

function registerSockets(app) {
    require("fs").readdirSync("./sockets").forEach(function(file) {
        if(file.match('js$')=='js') {
            var sock = require("./sockets/" + file);
            if(sock && sock.registerSocket) {
                sock.registerSocket(app);
                console.log(file + ': sockets registred');
            }
        }
    });
}

function initializeAuthentication() {

    everyauth.everymodule.findUserById(function (userId, callback) {
        UserHandler.findOne({_id: userId}, function(err, user) {
            callback(null, user);
        });
    });
    
    everyauth.password
        .getLoginPath('/login')
        .postLoginPath('/login')
        .loginView('login')
        .authenticate(function (login, password) {
            var promise = this.Promise()
            UserHandler.authenticate(login, password, function(err, user) {
               if(err) {
                   return promise.fulfill([err]);
               } else if(user) {   
                   return promise.fulfill(user)
               } else {
                   return promise.fulfill(['login failed']);
               }
            });
            return promise;
        })
        .loginSuccessRedirect('/')
        .getRegisterPath('/register')
        .postRegisterPath('/register')
        .registerView('register')
        .validateRegistration(function (newUserAttributes) {
            //TODO
            return [];
        })
        .registerUser(function(newUserAttributes) {
            var promise = this.Promise();
            UserHandler.register(newUserAttributes, function(err, createdUser) {
                if (err) {
                    return promise.fulfill([err]);
                }
                promise.fulfill(createdUser);
            });
            return promise;
        })
        .registerSuccessRedirect('/'); // Where to redirect to after a successful registration
    
    everyauth.helpExpress(app);
}

var app = module.exports = express.createServer(); //If https, it's inited here

initializeAuthentication();

app.configure(function() {
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.cookieParser());
    var RedisStore = require('connect-redis')(express);
    app.use(express.session({
        secret: config.get('sessionSecret') || 'P90Db4prHGTSY5NA6MyjZB6GmNm7nGjCiAyEAw5Uut6LpofOO5r01Kr65WaZ',
        store: new RedisStore(getRedisOptions())
    }));
    app.use(utils.apitoken.middleware());
    app.use(everyauth.middleware());
    app.use(express.static(__dirname + '/public'));
    
    everyauth.helpExpress(app);
});

app.configure('development', function() {
    everyauth.debug = true;
    
    app.use(express.errorHandler({
        dumpExceptions: true,
        showStack: true
    }));
    app.use(express.logger('dev'));
});

app.configure('production', function() {
    app.use(express.errorHandler());
    app.use(express.logger());
});

initializeDatabase();

registerRoutes(app);
registerSockets(app);

if (!module.parent) {
    app.listen(utils.serverPort);
    console.log('Express server listening on port %d in %s mode', utils.serverPort, app.settings.env);
}
