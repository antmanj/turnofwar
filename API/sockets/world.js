//Util is used for string interpolation
var util = require('util');
var sockIO = require('socket.io');

var World = require('../models/world.js');

module.exports = {
    registerSocket: function(app) {
		var io = sockIO.listen(app);
		io.of('/sock-world').on('connection', function (socket) {
			socket.on('generateWorld', function(data) {

				var chunk = World.GenerateChunk(data);
				
				for(var i=0;i<chunk.length;i++){
					socket.emit('drawRow', chunk[i]);
				}
				
				socket.emit('drawReady');
				console.log('Map generation ready, seed:' + data.seed);
			});
			console.log('New socket connection established.');
		});
    }
};