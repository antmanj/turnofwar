//
//Utils and thingies
//
var utils = module.exports = {};

utils.config = require('nconf');
utils.apitoken = require('./apitoken');
utils.config.argv().env();

var configFile = '../config.json';

if (utils.config.get('NODE_ENV')) {
    configFile = '../config-' + utils.config.get('NODE_ENV') + '.json';
}

utils.config.file({
    file: configFile
});

utils.serverPort = process.env.VCAP_APP_PORT || utils.config.get('server:port') || 1337;

utils.generateMongoUrl = function(options) {
    if (options.length > 0) {
        return options;
    } else {
        options.hostname = (options.hostname || 'localhost');
        options.port = (options.port || 27017);
        options.db = (options.db || 'TurnOfWar');
        if (options.username && options.password) {
            return "mongodb://" + options.username + ":" + options.password + "@" + options.hostname + ":" + options.port + "/" + options.db;
        }
        else {
            return "mongodb://" + options.hostname + ":" + options.port + "/" + options.db;
        }
    }
};
