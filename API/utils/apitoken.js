var connect = require('connect');
var token = require('../models/token');
var UserHandler = require('../models/user');

var apitoken = module.exports = {};

apitoken.middleware = function() {
    var app = connect(function registerReqGettersAndMethods(req, res, next) {
        next();
    },
    function fetchUserFromSession(req, res, next) {
        var tokenValue = req.header('ToW-access-token');
        if (tokenValue && tokenValue.length > 0) {
            token.getUserByToken(tokenValue, function(err, userid) {
                if (!err) {
                    UserHandler.findOne({_id: userid}, function(err, user) {
                        console.log(user.login + ' authenticated by token ' + tokenValue);
                        req.user = user;
                        req.session.userId = userid;
                        req.session.auth = {
                            userId: userid,
                            loggedIn: true
                        };
                        next();
                    });
                } else {
                    return next();
                }
            });
        }
        else {
            return next();
        }
    });
    return app;
};