//
//Based on tutorial on : http://blog.nodejitsu.com/rest-easy-test-any-api-in-nodejs
//
var APIeasy = require('api-easy');
var utils = require('../utils');
var assert = require('assert');

function createServerSuite(description) {
    return APIeasy.describe(description).use('127.0.0.1', utils.serverPort).discuss('Using server on port ' + utils.serverPort); 
}

function checkWarriorPosition(warrior, x, y) {
    assert.isNotNull(warrior);
    assert.isNotNull(warrior.pos);
    assert.equal(warrior.pos.x, x);
    assert.equal(warrior.pos.y, y);
}

function getAndCheckWarriorPos(id, x, y) {
    createServerSuite('GET')
    .discuss('getting warrior (id : '+id+')')
    .get('warrior/'+id)
    .expect('position should be ['+x+','+y+']', function (err, res, body) {
        assert.isNull(err);
        checkWarriorPosition(JSON.parse(body), x, y);
    }).run();
}

function getWarrior(id) {
    createServerSuite('Movement')
    .path('/warrior/' + id + '/move/')
    .put('left')
    .expect('position should be [9, 10]', function (err, res, body) {
        assert.isNull(err);
        getAndCheckWarriorPos(id, 9, 10);
    }).next()
    .put('up')
    .expect('position should be [9, 11]', function (err, res, body) {
        assert.isNull(err);
        getAndCheckWarriorPos(id, 9, 11);
    })
    .run();
}

createServerSuite('PUT')
.discuss('adding warrior to [10,10]')
.put('warrior/10/10')
.expect('position should be [10,10]', function (err, res, body) {
    assert.isNull(err);
    var testResult = JSON.parse(body); 
    checkWarriorPosition(testResult, 10, 10);
    getWarrior(testResult._id); 
}).run();


