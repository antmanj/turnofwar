//
//Based on tutorial on : http://blog.nodejitsu.com/rest-easy-test-any-api-in-nodejs
//
var APIeasy = require('api-easy');
var utils = require('../utils');
var assert = require('assert');

function createServerSuite(description) {
    return APIeasy.describe(description).use('127.0.0.1', utils.serverPort).discuss('Using server on port ' + utils.serverPort); 
}

createServerSuite('PUT')
.discuss('adding world')
.put('/world')
.expect('world seed should be something', function (err, res, body) {
}).run();


