function home(req, res) {
    res.render('home');
}

module.exports = {
    registerRoutes: function(app) {
        app.get('/', home);
    }
}