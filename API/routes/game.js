var CommonHandlers = require('./handlers.js');
var GameHandler = require('../models/game');
var handlers = require('./handlers');

function gameList(req, res) {
	GameHandler.listGames(req.user.login, function (err, game) {
		if(!err) {
			res.send(game);
		} else {
			//TODO: return error code
		}
	});
}

function gameGet(req, res) {
	GameHandler.getGame(req.params.id, function (err, game) {
		if(handlers.handleError(res, err, 'Game not found')) {
			if(game.owner == req.user.login) {
				res.send(game);
			} else {
			    handlers.error(res, 'Game not owned by user')
			}
		}
	});
}

function createGame(req, res) {
	GameHandler.createGame(req.user.login, function (err, game) {
		if(handlers.handleError(res, err, 'Game creation failed')) {
			res.send(game);
		}
	});
}

module.exports = {
    registerRoutes: function(app) {
        app.get('/game', CommonHandlers.requireLoggedIn, gameList);
        app.get('/game/:id', CommonHandlers.requireLoggedIn, gameGet);
        app.post('/game', CommonHandlers.requireLoggedIn, createGame);
    }
};
