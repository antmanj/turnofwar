/*
 * Common request handlers
 */
module.exports = {
    errorHandler: function (res, err, message) {
        if (err) {
            res.send({
                status: 1,
                message: message,
                err: err
            });
        }
        return err;
    },
    error: function(res, message) {
        
    },
    requireLoggedIn: function(req, res, next) {
       if(req.loggedIn) {
           return next();
       }
       return res.send({error: 'User nog logged in.'}, 401);
    }
}
