//Util is used for string interpolation
var World = require('../models/world.js');
var model = World.Model;

//Get one world
function worldGet(req,res) {
    return model.findById(req.params.id, function(err, world) {
        return res.send(world);
    });
}

//List all worlds
function worldList(req,res) {
    return model.find(function(err, worlds) {
        return res.send(worlds);
    });
}

function createWorld(req, res) {
    console.log("Creating world!");
    var data = World.GenerateChunk(req.body);
	return res.send(data);
}

module.exports = {
    registerRoutes: function(app) {
        app.get('/world', worldList);
        app.get('/world/:id', worldGet);
        app.post('/world/', createWorld);
    }
}
