var UserHandler = require('../models/user');
var TokenHandler = require('../models/token');
var handlers = require('./handlers');

function register(req, res) {
    UserHandler.register(req.body,
    function(err, createdUser) {
        if (handlers.handleError(res, err)) {
            res.send({
                status: 0,
                user: createdUser
            });
        }
    });
}

function requestToken(req, res) {
    UserHandler.authenticate(req.body.login, req.body.password,
    function(err, user) {
        if (handlers.handleError(res, err)) {
            TokenHandler.createTokenForUser(user._id,
            function(err, token) {
                if (handlers.handleError(res, err)) {
                    res.send({
                        status: 0,
                        token: token.token
                    });
                }
            })
        }
    });
}

module.exports = {
    registerRoutes: function(app) {
        app.post('/user/register', register);
        app.post('/user/token', requestToken);
    }
};