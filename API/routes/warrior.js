//Util is used for string interpolation
var util = require('util');
var CommonHandlers = require('./handlers.js');
var Warrior = require('../models/warrior.js').Model;

//Show one warrior
function warriorGet(req,res) {
    return Warrior.findById(req.params.id, function(err, warrior) {
        return res.send(warrior);
    });
}

//List all Warriors
function warriorList(req,res) {
    return Warrior.find(function(err, warriors) {
        return res.send(warriors);
    });
}

//Move Warrior one step in any direction
function warriorMove(req,res) {
    console.log(util.format("In warriorMove, id %s and direction %s", req.params.id, req.params.dir));

    var warrior = Warrior.findById(req.params.id, function (err, warrior){

        if (warrior) {

            if(!warrior.pos) {
                warrior.pos = {
                    x: 0,
                    y: 0
                };
            }

            switch (req.params.dir) {
                case "left":
                    warrior.pos.x -= 1;
                    break;
                case "right":
                    warrior.pos.x += 1;
                    break;
                case "down":
                    warrior.pos.y -= 1;
                    break;
                case "up":
                    warrior.pos.y += 1;
                    break;
            }
            warrior.save(function (err) {
                return res.send(util.format("Warrior '%s' position now %d,%d", warrior.name, warrior.pos.x, warrior.pos.y));
            });
        } else {
            console.log("Warrior not found: " + req.params.id);
            return res.send("NOT FOUND")
        }
    });
}

function createWarrior(req, res) {
    console.log("Creating warrior!");
    var newWarrior = new Warrior({
        name: 'Warrior', //TODO: name as parameter?
        owner: 'user', //TODO: Authed user
        pos:{
            x: req.params.x,
            y: req.params.y
        }
    });
    
    newWarrior.save(
        function (err, warrior) {
            console.log(warrior);
            return res.send(warrior);
        }
    );
}

module.exports = {
    registerRoutes: function(app) {
        app.get('/warrior/:id', warriorGet);
        app.put('/warrior/:id/move/:dir', warriorMove);
        app.put('/warrior/:x/:y', createWarrior);
        app.get('/warrior', warriorList);
    }
}
