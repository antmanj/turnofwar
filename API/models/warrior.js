// API Models
// Warrior

var mongoose = require('mongoose');

module.exports = {
    Model : mongoose.model('Warrior', new mongoose.Schema({
        name: { type: String, required: true },
        owner: { type: String, required: true },
        pos: {
            x: { type: Number },
            y: { type: Number }
        }
    }))
};

