var bcrypt = require('bcrypt'),
_schema = require('./schema'),
mongoose = require('mongoose');

var schema = new mongoose.Schema({});

schema.authenticate = function(login, password, callback) {
    this.findOne({login: login}, function(err, user) {
        if (err) {
            return callback(err);
        }
        if (!user) {
            return callback('User with login ' + login + ' does not exist');
        }
        user.authenticate(password, function(err, didSucceed) {
            if (err) {
                return callback(err);
            }
            if (didSucceed) {
                return callback(null, user);
            }
            return callback(null, null);
        });
    });
};

schema.register = function(userOpts, callback) {
    return this.create(userOpts, function(err, user) {
        if (err) {
            if (/duplicate key/.test(err)) {
                callback('Someone already has claimed that login.');
            }
            return callback(err, null);
        }
        return callback(null, user);
    });
};

schema.plugin(function(schema, opts) {
    schema.add(_schema);

    schema.virtual('password').get(function() {
        return this._password;
    }).set(function(password) {
        this._password = password;
        var salt = this.salt = bcrypt.genSaltSync(10);
        this.hash = bcrypt.hashSync(password, salt);
    });

    schema.method('authenticate', function(password, callback) {
        bcrypt.compare(password, this.hash, callback);
    });
    
    schema.static('authenticate', schema.authenticate);
    schema.static('register', schema.register);
});

module.exports = mongoose.model('Users', schema);