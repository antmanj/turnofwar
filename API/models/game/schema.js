module.exports = { 
    seed: { type: String },
    owner: { type: String },
    created: { type: Date, default: Date.now}
};