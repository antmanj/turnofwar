var _schema = require('./schema'),
mongoose = require('mongoose');

var schema = new mongoose.Schema({});

schema.createGame = function(owner, cb) {
    var newGame = new this({
        seed: Math.floor(Math.random() * 0xFFFFFF),
        owner: owner
    });

    newGame.save(function(err, game) {
        if (err) {
            return cb(err, game);
        }

        console.log(game);

        if (cb) {
            return cb(null, game);
        }
    }
    );
};

schema.listGames = function(owner, cb) {
    this.find({ owner: owner }, function(err, game) { cb(err, game); });
};

schema.getGame = function(id, cb) {
    this.findById(id,
    function(err, game) {
        cb(err, game);
    });
};

schema.plugin(function(schema, opts) {
    schema.add(_schema);
    
    schema.static('createGame', schema.createGame);
    schema.static('listGames', schema.listGames);
    schema.static('getGame', schema.getGame);
});

module.exports = mongoose.model('Games', schema);