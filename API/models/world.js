// API Models
// World

//TODO: Move to world/index.js & schema.js

var mongoose = require('mongoose');
var SimplexNoise = require('../algorithms/simplex-noise.js');
var MersenneTwister = require('../algorithms/mersenne-twister.js');

function normalizeValue(a1, a2, b1, b2, num) {
	var value = ((num - a1) * (b2 -b1)/(a2 - a1)) + b1;
	
	if(value > 160) {
		value = 160;
	} 
	else if(value > 150) {
		value = 150;
	}
	else if(value > 145) {
		value = 145;
	}
	else if(value > 140) {
		value = 140;
	}
	else {
		value = 0;
	}
	
	return value;
}

module.exports = {
    Model : mongoose.model('World', new mongoose.Schema({
        name: { type: String, required: true },
        seed: { type: String},
        size: {
            height: { type: Number },
            width: { type: Number }
        },
        data: { type: Array }
    })),
	GenerateChunk: function(data) {
		var noiseGenerator = new SimplexNoise(new MersenneTwister(data.seed));
		
		var width = parseInt(data.width) || 100;
		var height = parseInt(data.height) || 100;
		var zoom = data.zoom || 1.5;
		var zOff = data.zOffset || 0;
		var map = [];
		
		var x = parseInt(data.x) || 0;
		var yInitial = parseInt(data.y) || 0;
		var xInitial = x;
		
		var xMax = x+width;
		var yMax = yInitial+height;
		
		for (; x < xMax; x++){
			var xVal = (x/(xInitial-xMax)) * zoom;
			var row = [];
			for (var y=yInitial; y < yMax; y++){
				var yVal = (y/(yInitial-yMax)) * zoom;
				var result = noiseGenerator.noise3d(xVal, yVal, zOff);
				result = normalizeValue(-1, 1, 0, 255, result);
				if(result > 0) {
					row.push({ x : x, y: y, res: result });
				}
			}
			map.push(row);
		}
		return map;
	}
};

