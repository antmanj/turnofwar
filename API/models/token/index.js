var _schema = require('./schema'),
mongoose = require('mongoose'),
crypto = require('crypto');

var schema = new mongoose.Schema({});

schema.getUserByToken = function(token, callback) {
    this.findOne({
        token: token
    },
    function(err, token) {
        if (err) {
            return callback(err);
        }
        if (!token) {
            return callback('Invalid token: ' + token);
        }
        return callback(null, token.user);
    });
};

schema.createTokenForUser = function(userid, callback) {
    var obj = {
        user: userid
    };
    var $this = this;
    return crypto.randomBytes(48, function(ex, buf) {
        obj.token = buf.toString('hex');
        return $this.create(obj ,
               function (err, token) {
               if (err) {
                   return callback(err, null);
               }
               return callback(null, obj);
           });
    });
};

schema.plugin(function(schema, opts) {
    schema.add(_schema);
    schema.static('createTokenForUser', schema.createTokenForUser);
    schema.static('getUserByToken', schema.getUserByToken);
});

module.exports = mongoose.model('Token', schema);