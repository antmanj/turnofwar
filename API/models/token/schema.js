module.exports = {
    token: { type: String, unique: true },
    user: String,
    created:  {type: Date, default: Date.now}
};